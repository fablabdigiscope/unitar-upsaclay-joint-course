
# UNITAR | FABLAB UPSACLAY
---
###### Possible Cross-Overs between :
###### ```MASTER OF SCIENCE IN INTERNATIONAL MANAGEMENT – RESPONSIBLE MANAGEMENT AND CLIMATE CHANGE``` and ```DU FAB ACADEMY+```
---
###### Peoples involved : Jonas Haertle (UNITAR), Romain Di Vozzo (UPSACLAY),...
---
###### Several degrees of collaboration seem possible :
- 1) According to relevance between __different__ but __related topics__,
- 2 )Acccording to the __courses's common schedule__ along the year,
- 3) According to __same topics__ with different but __complementary approaches__.
- ?
---

###### General Information :

| Details | UNITAR | UPSACLAY |
| - | - | - |
| **When ?** | From September to July | From September to September |
| **Where ?** | Lugano + Geneva (SZ) | Gif-sur-yvette (FR) |
| **Degree ?** | Master | DU (Master Equivalent) |
| **Internship** | ? | YES |
| **Number of Students** | ? | 2 to 5 |
| **Fee/Tuition** | ? | From 10k (graduates) to 12k (vocational) |
| **External Partners** | ? | The Fab Foundation (US) |

---

## MATCHING CLASSES TOGETHER ACCORDING TO RELEVANCE

---

### 1) CROSS-VOVER OPPORTUNITY 1 : ```DIFFERENT BUT RELATED```

| | | | | | |
| :- | - | :---------------------- | - | -: | -: |
| **UNITAR** | | **Cross-over Opportunities** | | **UPSACLAY** |
| | | | |
| **MSIM Courses** |  | Different but Related | | **DU FAB ACADEMY+ Classes** | **DU FAB ACADEMY+ Classes (in french)** |
| Leadership Development through Action Learning | 1 | | 1 | Principles and Practices | Principes et Usages |
| International Management | 2 | | 2 | Project Management | Management de Projet |
| Digital Transformation Strategies | 3 | | 3 | Computer Aided Design | Design Assisté par Ordinateur |
| Innovation and Project Management | 4 | | 4 |Computer-Controlled Cutting | Découpe Controlée par Ordinateur |
| Challenges in the New Global Marketplace | 5 | | 5 | Electronics Production | Fabrication Électronique |
| Global Issues and Responsible Leadership | 6 | | 6 | 3D Scanning and 3D Printing | Scanner 3D et Impression 3D |
| International Business Economics | 7 | | 7 | Computer Controlled Milling | Fraisage Contrôlé par Ordinateur |
| Innovation and Design Thinking | 8 | | 8 | Embedded Programming | Programmation Intégrée |
| The Future of Work | 9 |  | 9 | Molding and Casting | Moulage et Coulage |
| Organization Evolution and Design | 10 | | 10 | Input Devices | Dispositifs Électroniques d'Entrée |
| Career Strategies | 11 | | 11 | Output Devices | Dispositifs Électroniques de Sortie |
| Capstone Learning ePortfolio | 12 | | 12 | Networks and Communications | Réseaux et Communications |
| Capstone Learning ePortfolio Part II | 13 | | 13 | Mechanical Design | Design Mécanique |
| **Responsible Management and Climate Action** (as a group of classes)| 14 | | 14 | Interface Application and Programming | Programmation d'Interface et d'Applications |
| UNITAR Responsible Management and Climate Action | 15 | | 15 | Machine Design | Design de Machine |
| Practical Experience::Climate Action | 16 | | 16 | Wild-Card Week | Carte Blanche |
| **Digital Transformation** (as a group of classes) | 17 | | 17 | Invention, Intellectual Property, and Income | Invention, Propriété Intellectuelle, et Revenus |
| Digital Transformation Practices | 18 | | 18 | Applications and Implications | Applications et Implications |
| Practical Experience-Digital Transformation | 19 | | 19 | Project Development | Développement de Projet |

---

### 2) CROSS-VOVER OPPORTUNITY 2 : ```SCHEDULED AT THE SAME TIME```

| | | | | | |
| :- | - | :---------------------- | - | -: | -: |
| **UNITAR** | | **Cross-over Opportunities** | | **UPSACLAY** |
| | | | |
| **MSIM Courses** |  | Scheduled at the same time | | **DU FAB ACADEMY+ Classes** | **DU FAB ACADEMY+ Classes (in french)** |
| Leadership Development through Action Learning | 1 | | 1 | Principles and Practices | Principes et Usages |
| International Management | 2 | | 2 | Project Management | Management de Projet |
| Digital Transformation Strategies | 3 | | 3 | Computer Aided Design | Design Assisté par Ordinateur |
| Innovation and Project Management | 4 | | 4 |Computer-Controlled Cutting | Découpe Controlée par Ordinateur |
| Challenges in the New Global Marketplace | 5 | | 5 | Electronics Production | Fabrication Électronique |
| Global Issues and Responsible Leadership | 6 | | 6 | 3D Scanning and 3D Printing | Scanner 3D et Impression 3D |
| International Business Economics | 7 | | 7 | Computer Controlled Milling | Fraisage Contrôlé par Ordinateur |
| Innovation and Design Thinking | 8 | | 8 | Embedded Programming | Programmation Intégrée |
| The Future of Work | 9 |  | 9 | Molding and Casting | Moulage et Coulage |
| Organization Evolution and Design | 10 | | 10 | Input Devices | Dispositifs Électroniques d'Entrée |
| Career Strategies | 11 | | 11 | Output Devices | Dispositifs Électroniques de Sortie |
| Capstone Learning ePortfolio | 12 | | 12 | Networks and Communications | Réseaux et Communications |
| Capstone Learning ePortfolio Part II | 13 | | 13 | Mechanical Design | Design Mécanique |
| **Responsible Management and Climate Action** (as a group of classes)| 14 | | 14 | Interface Application and Programming | Programmation d'Interface et d'Applications |
| UNITAR Responsible Management and Climate Action | 15 | | 15 | Machine Design | Design de Machine |
| Practical Experience::Climate Action | 16 | | 16 | Wild-Card Week | Carte Blanche |
| **Digital Transformation** (as a group of classes) | 17 | | 17 | Invention, Intellectual Property, and Income | Invention, Propriété Intellectuelle, et Revenus |
| Digital Transformation Practices | 18 | | 18 | Applications and Implications | Applications et Implications |
| Practical Experience-Digital Transformation | 19 | | 19 | Project Development | Développement de Projet |

---

### 3) CROSS-VOVER OPPORTUNITY 3 : ```SAME TOPIC BUT DIFFERENT APPROACH```

| | | | | | |
| :- | - | :---------------------- | - | -: | -: |
| **UNITAR** | | **Cross-over Opportunities** | | **UPSACLAY** |
| | | | |
| **MSIM Courses** |  | Same Topic but Different Approach | | **DU FAB ACADEMY+ Classes** | **DU FAB ACADEMY+ Classes (in french)** |
| Leadership Development through Action Learning | 1 | | 1 | Principles and Practices | Principes et Usages |
| International Management | 2 | | 2 | Project Management | Management de Projet |
| Digital Transformation Strategies | 3 | | 3 | Computer Aided Design | Design Assisté par Ordinateur |
| Innovation and Project Management | 4 | | 4 |Computer-Controlled Cutting | Découpe Controlée par Ordinateur |
| Challenges in the New Global Marketplace | 5 | | 5 | Electronics Production | Fabrication Électronique |
| Global Issues and Responsible Leadership | 6 | | 6 | 3D Scanning and 3D Printing | Scanner 3D et Impression 3D |
| International Business Economics | 7 | | 7 | Computer Controlled Milling | Fraisage Contrôlé par Ordinateur |
| Innovation and Design Thinking | 8 | | 8 | Embedded Programming | Programmation Intégrée |
| The Future of Work | 9 |  | 9 | Molding and Casting | Moulage et Coulage |
| Organization Evolution and Design | 10 | | 10 | Input Devices | Dispositifs Électroniques d'Entrée |
| Career Strategies | 11 | | 11 | Output Devices | Dispositifs Électroniques de Sortie |
| Capstone Learning ePortfolio | 12 | | 12 | Networks and Communications | Réseaux et Communications |
| Capstone Learning ePortfolio Part II | 13 | | 13 | Mechanical Design | Design Mécanique |
| **Responsible Management and Climate Action** (as a group of classes)| 14 | | 14 | Interface Application and Programming | Programmation d'Interface et d'Applications |
| UNITAR Responsible Management and Climate Action | 15 | | 15 | Machine Design | Design de Machine |
| Practical Experience::Climate Action | 16 | | 16 | Wild-Card Week | Carte Blanche |
| **Digital Transformation** (as a group of classes) | 17 | | 17 | Invention, Intellectual Property, and Income | Invention, Propriété Intellectuelle, et Revenus |
| Digital Transformation Practices | 18 | | 18 | Applications and Implications | Applications et Implications |
| Practical Experience-Digital Transformation | 19 | | 19 | Project Development | Développement de Projet |
